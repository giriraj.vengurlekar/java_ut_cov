package org.common;

import org.mainapp.Constants;

public class Logger {

    public static void logInfo(String info){
        System.out.println(info);
    }

    public static void logErr(String err){
        System.err.println(err);
    }

    public static void logGlobals(){
        System.err.println( Constants.globalConst );
    }
}
