package org.mainapp;

import org.common.Logger;

import java.util.ArrayList;

public class Employee {

        private String id;
        private String name;
        private Department department;
        private int age;

        public Employee(){
        }

        public Employee(String id, String name, Department department){
            this.id = id;
            this.name = name;
            this.department = department;
        }

        //constructor with age argument
        public Employee(String id, String name, Department department, int age){
            this.id = id;
            this.name = name;
            this.department = department;
            this.age = age;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Department getDepartment() {
            return department;
        }

        public void setDepartment(Department department) {
            this.department = department;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String toString(){

            return "[" +
                    this.getId() +
                    " : " +
                    this.getName() +
                    " : " +
                    this.getDepartment() +
                    "]";
        }

        public ArrayList<Employee> fetchEmployees() {


            ArrayList<Employee> arr = new ArrayList<>(10);
            for (int i=0; i<5; i++){
                Employee e = new Employee();
                Department dept = new Department("HR", i);
                e.setId( Integer.toString(i));
                e.setName( "org.mainapp.Employee" + i);
                e.setDepartment(dept);
                arr.add(e);
            }
            return arr;
        }

        public void dumpDetails(){

            Logger.logInfo(this.getName());
    }

}
