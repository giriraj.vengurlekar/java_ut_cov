package org.mainapp;

import org.common.Logger;

public class Department {

    private String deptName;
    private Integer id;
    private PermEmployee pe;  // This is the dependency that makes it a cycle


    public Department(String name, Integer id){
        deptName = name;
        id  = id;

    }

    public void setId(int id){
        id = id;
    }

    public void setName(String name){
        deptName = name;
    }

    public Integer getId(){
        return id;
    }

    public String getDeptName(){
        return deptName;
    }

    public String toString(){
        return deptName;
    }

    public PermEmployee getPe() {
        return pe;
    }

    public void setPe(PermEmployee pe) {
        this.pe = pe;
    }

    public void dumpDetails(){
        Logger.logInfo( deptName );
    }
}
