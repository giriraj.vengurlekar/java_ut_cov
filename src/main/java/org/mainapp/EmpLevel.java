package org.mainapp;

public enum EmpLevel {

    Junior_Engineer,
    Senior_Engineer,
    Manager,
    Director,
    Vice_President

}
